# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.tests.test_tryton import ModuleTestCase


class CarrierWeightVolumeCombinedTestCase(ModuleTestCase):
    "Test Carrier Weight Volume Combined module"
    module = 'carrier_weight_volume_combined'


del ModuleTestCase
